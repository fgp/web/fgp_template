<?php
namespace DWenzel\FgpTemplate\DataProcessing;
/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Class for data processing for the custom content element "Text"
 */
class ContentElementTextProcessor implements DataProcessorInterface
{
    const KEYS_TO_MAP = [
        'uid' => 'uid',
        'header' => 'title',
        'bodytext' => 'text',
        'subheader' => 'subTitle'
    ];

    /**
     * Process data for the content element "My new content element"
     *
     * @param ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    )
    {
        $keys = static::KEYS_TO_MAP;
        foreach ($keys as $sourceKey => $targetKey) {
            if (!empty($processedData['data'][$sourceKey])) {
                $processedData[$targetKey] = $processedData['data'][$sourceKey];
            }
        }

        return $processedData;
    }
}