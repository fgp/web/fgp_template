<?php

namespace DWenzel\FgpTemplate\Twig\Loader;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use DWenzel\FgpTemplate\Configuration\SettingsInterface as SI;
use Twig\Loader\FilesystemLoader;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ResourcePathLoader
 */
class RecursivePathLoader extends FilesystemLoader
{
    const ROOT_PATH = SI::PRIVATE_RESOURCES_PATH . DIRECTORY_SEPARATOR . 'Default' . DIRECTORY_SEPARATOR;

    const DIRECTORIES = [
        'ContentElements/Twig',
        'Page/Partials'
    ];

    const MAX_DEPTH = 10;

    /**
     * ResourcePathLoader constructor.
     * @param array $paths
     * @param null $rootPath
     * @throws \Twig_Error_Loader
     * @throws TooManyRecursionsException
     */
    public function __construct($paths = array(), $rootPath = null)
    {
        if (null === $rootPath) {
            $rootPath = GeneralUtility::getFileAbsFileName(
                'EXT:' . SI::EXTENSION_NAME . DIRECTORY_SEPARATOR . static::ROOT_PATH
            );
        }
        parent::__construct($paths, $rootPath);
        foreach (static::DIRECTORIES as $directory) {
            if ($absolutePath = GeneralUtility::getFileAbsFileName($rootPath . $directory)) {
                /** @var \RecursiveDirectoryIterator $directoryIterator */
                $directory = new \RecursiveDirectoryIterator($absolutePath);
                $iterator = new \RecursiveIteratorIterator($directory, \RecursiveIteratorIterator::SELF_FIRST);
                foreach ($iterator as $item) {
                    if ($iterator->getDepth() > static::MAX_DEPTH) {
                        throw new TooManyRecursionsException(
                            sprintf('Maximum recursion depth of %s reached. Can not load templates.', static::MAX_DEPTH),
                            1539545133
                        );
                    }
                    if (!$item->isDir()) {
                        continue;
                    }
                    $this->addPath($item->getRealPath());
                }
            }
        }
    }

}