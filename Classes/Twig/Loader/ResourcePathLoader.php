<?php

namespace DWenzel\FgpTemplate\Twig\Loader;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use DWenzel\FgpTemplate\Configuration\SettingsInterface as SI;
use Twig\Loader\FilesystemLoader;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ResourcePathLoader
 */
class ResourcePathLoader extends FilesystemLoader
{
    const ROOT_PATH = SI::PRIVATE_RESOURCES_PATH . DIRECTORY_SEPARATOR . 'Default' . DIRECTORY_SEPARATOR;

    const PATHS = [
        'Page/Layouts',
        'Page/Partials',
        'Page/Templates',
        'ContentElements/Templates',
        'ContentElements/Partials'
    ];

    /**
     * ResourcePathLoader constructor.
     * @param array $paths
     * @param null $rootPath
     * @throws \Twig_Error_Loader
     */
    public function __construct($paths = array(), $rootPath = null)
    {
            if (null === $rootPath) {
            $rootPath = GeneralUtility::getFileAbsFileName(
                'EXT:' . SI::EXTENSION_NAME . DIRECTORY_SEPARATOR . static::ROOT_PATH
            );
        }
        parent::__construct($paths, $rootPath);
        foreach (static::PATHS as $path) {
            $this->addPath($path);
     }
    }
}