<?php
defined('TYPO3_MODE') || die();

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'LLL:EXT:fgp_template/Resources/Private/Language/Tca.xlf:fgptemplate_ceText',
        'fgptemplate_ceText',
        'EXT:fgp_template/Resources/Public/Icons/ContentElements/fgptemplate_ceText.gif'
    ),
    'CType',
    'fgp_template'
);

// Configure the default backend fields for the content element
$GLOBALS['TCA']['tt_content']['types']['fgptemplate_ceText'] = array(
    'showitem' => '
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.general;general,
         header,subheader,bodytext,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:tabs.access,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.visibility;visibility,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.access;access,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:tabs.extended
');