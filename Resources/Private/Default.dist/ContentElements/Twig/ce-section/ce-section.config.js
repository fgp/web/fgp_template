module.exports = {
    //title: 'CE Section',
    status: 'out',

    collated: false,
    default: 'wide',
    context: {
        color: 'g1',
        sectionHeadline: 'Section Title'
    },
    variants: [
        {
            name: 'narrow',
            context: {
                sectionHeadline: 'Section Narrow',
                narrow: true
            }
        }, {
            name: 'wide',
            context: {
                sectionHeadline: 'Section Wide'
            }
        }
    ]
};
