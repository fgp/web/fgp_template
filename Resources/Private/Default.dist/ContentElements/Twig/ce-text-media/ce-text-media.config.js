module.exports = {
    //title: 'CE Text Media',
    status: 'wip',

    collated: false,
    default: 'left',
    context: {
        spaceAfter: '@data.spaceAfter.default',
        textS: '@data.text.paragraph-s',
        textM: '@data.text.paragraph-m',
        textL: '@data.text.paragraph-l',
    },
    variants: [
        {
            name: 'above',
            context: {
                modifier: 'above',
                title: 'Text Media: image above',
            }
        },
        {
            name: 'below',
            status: 'out',
            context: {
                below: true,
                modifier: 'below',
                title: 'Text Media: image below',
            }
        },
        {
            name: 'left',
            status: 'out',
            context: {
                imageSmall: true,
                modifier: 'left',
                title: 'Text Media: image left'
            }
        },
        {
            name: 'right',
            context: {
                imageSmall: true,
                modifier: 'right',
                title: 'Text Media: image right'
            }
        }
    ]
};
