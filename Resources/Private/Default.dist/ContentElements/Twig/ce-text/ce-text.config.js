module.exports = {
    //title: 'CE Text',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        uid: '14',
        spaceAfter: '@data.spaceAfter.default',
        title: 'Text',
        subTitle: '@data.text.paragraph-m',
        text:    '@data.text.paragraph-l'
    }
};
