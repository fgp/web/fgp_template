<?php

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['hellurl']['DEFAULT'] = array(
    'init' => array(
        'enableCHashCache' => true,
        'appendMissingSlash' => 'ifNotFile,redirect',
        'adminJumpToBackend' => true,
        'enableUrlDecodeCache' => true,
        'enableUrlEncodeCache' => true,
        'emptyUrlReturnValue' => '/',
    ),
    'pagePath' => array(
        'type' => 'user',
        'userFunc' => 'Nimut\\Hellurl\\UriGeneratorAndResolver->main',
        'spaceCharacter' => '-',
        'languageGetVar' => 'L',
        'rootpage_id' => '1',
    ),
    'fileName' => array(
        'defaultToHTMLsuffixOnPrev' => 0,
        'acceptHTMLsuffix' => 1,
        'index' => array(
            'sitemap.xml' => array(
                'keyValues' => array(
                    'type' => 1449874941,
                ),
            ),
        ),
    ),
    'preVars' => array(
        0 => array(
            'GETvar' => 'L',
            'valueMap' => array(
                //'en' => 1,
            ),
            'noMatch' => 'bypass',
        ),
    ),
    'fixedPostVars' => array(
        /*
        'newsDetailConfiguration' => array(
            array(
                'GETvar' => 'tx_news_pi1[news]',
                'lookUpTable' => array(
                    'table' => 'tx_news_domain_model_news',
                    'id_field' => 'uid',
                    'alias_field' => 'title',
                    'addWhereClause' => ' AND NOT deleted AND NOT hidden',
                    'useUniqueCache' => 1,
                    'useUniqueCache_conf' => array(
                        'strtolower' => 1,
                        'spaceCharacter' => '-',
                    ),
                    'languageGetVar' => 'L',
                    'languageExceptionUids' => '',
                    'languageField' => 'sys_language_uid',
                    'transOrigPointerField' => 'l10n_parent',
                    'autoUpdate' => 1,
                    'expireDays' => 180,
                ),
            ),
            array(
                'GETvar' => 'tx_news_pi1[action]',
                'noMatch' => 'bypass'
            ),
            array(
                'GETvar' => 'tx_news_pi1[controller]',
                'noMatch' => 'bypass'
            ),
        ),
        'newsCategoryConfiguration' => array(
            array(
                'GETvar' => 'tx_news_pi1[overwriteDemand][categories]',
                'lookUpTable' => array(
                    'table' => 'sys_category',
                    'id_field' => 'uid',
                    'alias_field' => 'title',
                    'addWhereClause' => ' AND NOT deleted',
                    'useUniqueCache' => 1,
                    'useUniqueCache_conf' => array(
                        'strtolower' => 1,
                        'spaceCharacter' => '-'
                    )
                )
            ),
        ),
        */
    ),
    'postVarSets' => array(
        'DEFAULT' => array(
            /*
            'news' => array(
                array(
                    'GETvar' => 'tx_news_pi1[@widget_0][currentPage]',
                ),
            ),
            */
        ),
    ),
);

foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['hellurl'] as $key => &$value) {
    if ('DEFAULT' === $key) {
        continue;
    }
    $value = array_replace_recursive($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['hellurl']['DEFAULT'], $value);
}
