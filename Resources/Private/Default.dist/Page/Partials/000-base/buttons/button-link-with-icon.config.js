module.exports = {
    //title: 'Button link with icon',
    status: 'wip',

    collated: false,
    default: 'link',    // default will now be the variant with the name 'link'
    context: {},        // no shared context
    variants: [
        {
            name: 'link',
            context: {}
        },
        {
            name: 'link-disabled',
            context: {
                buttonDisabled: true
            }
        }
    ]
};
