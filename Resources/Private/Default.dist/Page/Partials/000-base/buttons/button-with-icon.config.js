module.exports = {
    //title: 'Button with icon',
    status: 'wip',

    collated: false,
    default: 'primary', // default will now be the variant with the name 'primary'
    context: {},        // no shared context
    variants: [
        {
            name: 'primary',
            context: {
                buttonModifier: 'primary'
            }
        },
        {
            name: 'primary-disabled',
            context: {
                buttonDisabled: true,
                buttonModifier: 'primary'
            }
        },
        {
            name: 'secondary',
            context: {
                buttonModifier: 'secondary'
            }
        },
        {
            name: 'secondary-Disabled',
            context: {
                buttonDisabled: true,
                buttonModifier: 'secondary'
            }
        },
        {
            name: 'positive',
            context: {
                buttonModifier: 'positive'
            }
        },
        {
            name: 'positive-Disabled',
            context: {
                buttonDisabled: true,
                buttonModifier: 'positive'
            }
        },
        {
            name: 'caution',
            context: {
                buttonModifier: 'caution'
            }
        },
        {
            name: 'caution-Disabled',
            context: {
                buttonDisabled: true,
                buttonModifier: 'caution'
            }
        },
        {
            name: 'negative',
            context: {
                buttonModifier: 'negative'
            }
        },
        {
            name: 'negative-Disabled',
            context: {
                buttonDisabled: true,
                buttonModifier: 'negative'
            }
        },
        {
            name: 'ghost',
            context: {
                buttonModifier: 'ghost'
            }
        },
        {
            name: 'ghost-Disabled',
            context: {
                buttonDisabled: true,
                buttonModifier: 'ghost'
            }
        }
    ]
};
