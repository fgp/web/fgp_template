module.exports = {
    //title: 'Buttons all',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        primary: {
            buttonDisabled: false,
            buttonModifier: 'primary',
            buttonLabel: 'Button primary'
        },
        primaryDisabled: {
            buttonDisabled: true,
            buttonModifier: 'primary',
            buttonLabel: 'Button primary disabled'
        },
        secondary: {
            buttonModifier: 'secondary',
            buttonLabel: 'Button secondary'
        },
        secondaryDisabled: {
            buttonDisabled: true,
            buttonModifier: 'secondary',
            buttonLabel: 'Button secondary disabled'
        },
        positive: {
            buttonModifier: 'positive',
            buttonLabel: 'Button positive'
        },
        positiveDisabled: {
            buttonDisabled: true,
            buttonModifier: 'positive',
            buttonLabel: 'Button positive disabled'
        },
        caution: {
            buttonModifier: 'caution',
            buttonLabel: 'Button caution'
        },
        cautionDisabled: {
            buttonDisabled: true,
            buttonModifier: 'caution',
            buttonLabel: 'Button caution disabled'
        },
        negative: {
            buttonModifier: 'negative',
            buttonLabel: 'Button negative'
        },
        negativeDisabled: {
            buttonDisabled: true,
            buttonModifier: 'negative',
            buttonLabel: 'Button negative disabled'
        },
        ghost: {
            buttonModifier: 'ghost',
            buttonLabel: 'Button ghost'
        },
        ghostDisabled: {
            buttonDisabled: true,
            buttonModifier: 'ghost',
            buttonLabel: 'Button ghost disabled'
        },
        small: {
            buttonModifier: 'small c-button--primary',
            buttonLabel: 'Button small'
        },
        large: {
            buttonModifier: 'large c-button--primary',
            buttonLabel: 'Button large'
        },
        link: {
            buttonModifier: 'link',
            buttonLabel: 'Button link'
        },
        linkDisabled: {
            buttonDisabled: true,
            buttonModifier: 'link',
            buttonLabel: 'Button link disabled'
        }
    }
};
