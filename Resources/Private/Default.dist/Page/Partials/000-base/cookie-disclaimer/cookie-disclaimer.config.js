module.exports = {
    title: 'Cookie Disclaimer',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        text: 'Wir verwenden Cookies, um die angebotenen Funktionen für Sie zu verbessern und auf Sie zugeschnittene Inhalte bieten zu können. Indem Sie mit der Navigation auf unserer Website fortfahren, erklären Sie sich mit der Verwendung von Cookies einverstanden.',
        buttonData: {
            typo: '@data.typo.bold',
            buttonModifier: 'secondary c-cookie-disclaimer__button js-cookie-disclaimer-toggle',
            buttonLabel: 'OK'
        }
    }
};
