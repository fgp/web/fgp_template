module.exports = {
    //title: 'Footer',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        menuMetaData: {
            ariaLabel: 'Footer-Navigation',
            headline: false,
            menuMetaItems: [
                {
                    current: false,
                    title: 'Impressum',
                    url: '#'
                }, {
                    title: 'Kontakt',
                    url: '#'
                }, {
                    title: 'Datenschutz',
                    url: '#'
                }
            ]
        },
        copyright: '@data.text.copyright'
    }
};
