module.exports = {
    //title: 'Form example',
    status: 'out',

    collated: false,
    default: 'default',
    context: {
        spaceAfter: '@data.spaceAfter.default',
        formTitle: 'Form Title',
        fieldsetLegend: 'Fieldset',
        text: '@data.text.paragraph-l',
        inputNoDecriptionData: {
            id: '1',
            required: false,
            described: false
        },
        inputData: {
            id: '2',
            placeholder: false,
            descriptionId: 2
        },
        inputIconData: {
            id: '3',
            icon: true,
            placeholder: false,
            described: 'Feld mit Icon',
            descriptionId: 3
        },
        inputHiddenLabelData: {
            id: '4',
            hiddenClass: true,
            described: 'Feld mit versteckter Bezeichnung',
            descriptionId: 4,
            required: false,
            icon: false
        },
        inputErrorData: {
            id: '5',
            described: 'Hinweistext mit Fehlerbeschreibung',
            descriptionId: 5,
            isError: true
        },
        inputValidData: {
            id: '6',
            described: 'Möglicher Text bei korrekter Eingabe',
            descriptionId: 6,
            isValid: true
        },
        buttonSubmitData: {
            buttonModifier: 'primary',
            buttonLabel: 'Absenden',
            typeButton: false,
            typeSubmit: true,
            valueSubmit: 'send'
        },
        buttonResetData: {
            buttonModifier: 'ghost',
            buttonLabel: 'Zurücksetzen',
            typeButton: false,
            typeReset: true,
            valueReset: 'reset'
        }
    }
};
