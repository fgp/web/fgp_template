module.exports = {
    //title: 'Form field checkbox',
    status: 'out',

    collated: false,
    default: 'default',
    context: {
        fieldsetLegend: 'Bezeichnung für Checkboxen',
        isValid: false,
        isError: false,
        textS: '@data.text.paragraph-s',
        textM: '@data.text.paragraph-m'
    }
};
