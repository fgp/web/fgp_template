module.exports = {
    //title: 'Form field input',
    status: 'out',

    collated: false,
    default: 'default',
    context: {
        id: '1',
        isValid: false,
        isError: false,
        hiddenClass: false,
        label: 'Feld-Bezeichnung',
        required: true,
        //type: 'file'
        placeholder: 'Platzhaltertext',
        icon: false,
        described: 'Platz für Hinweistext...',
        descriptionId: 1
    }
};
