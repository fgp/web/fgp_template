module.exports = {
    //title: 'Form field radio',
    status: 'out',

    collated: false,
    default: 'default',
    context: {
        fieldsetLegend: 'Bezeichnung für Radio-Buttons',
        isValid: false,
        isError: false,
        textS: '@data.text.paragraph-s',
        textM: '@data.text.paragraph-m'
    }
};
