module.exports = {
    //title: 'Form field select',
    status: 'out',

    collated: false,
    default: 'default',
    context: {
        id: '1',
        isValid: false,
        isError: false,
        hiddenClass: false,
        label: 'Feld-Bezeichnung',
        placeholder: 'Bitte wählen Sie eine Option',
        required: true,
        described: 'Platz für Hinweistext',
        descriptionId: 1
    }
};
