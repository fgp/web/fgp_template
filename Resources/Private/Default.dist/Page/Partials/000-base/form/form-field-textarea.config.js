module.exports = {
    //title: 'Form field textarea',
    status: 'out',

    collated: false,
    default: 'default',
    context: {
        id: '1',
        isValid: false,
        isError: false,
        hiddenClass: false,
        label: 'Feld-Bezeichnung',
        placeholder: 'Ihr Feedback...',
        required: true,
        described: 'Platz für Hinweistext',
        descriptionId: 1
    }
};
