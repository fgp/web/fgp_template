module.exports = {
    //title: 'Header',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        spaceAfter: '@data.spaceAfter.small',
        menuMainItems: [
            {
                current: true,
                title: 'Schule',
                url: '#'
            },
            {
                title: 'Konzept',
                url: '#'
            }, {
                title: 'Team',
                url: '#'
            }, {
                title: 'Träger',
                url: '#'
            }, {
                title: 'Formelles',
                url: '#'
            }
        ]
    }
};
