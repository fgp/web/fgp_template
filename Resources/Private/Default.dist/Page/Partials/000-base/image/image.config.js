module.exports = {
    //title: 'Image',
    status: 'wip',

    collated: false,
    default: '16x9',
    context: {
        imgS: '@data.img.16x9-s',
        imgM: '@data.img.16x9-m',
        imgL: '@data.img.16x9-l',
        caption: '@data.text.paragraph-m',
        copyright: '@data.text.copyright'
    },
    variants: [
        {
            name: '16x9-small',
            context: {
                imageSmall: true,
                lightbox: true
            }
        },
        {
            name: '16x9-small-no-lightbox',
            context: {
                imageSmall: true,
                lightbox: false
            }
        },
        {
            name: '16x9',
            context: {
                lightbox: true
            }
        },
        {
            name: '16x9-no-lightbox',
            context: {
                lightbox: false
            }
        }
    ]
};
