module.exports = {
    title: 'Link styled like button',
    status: 'wip',

    collated: false,
    default: 'primary',
    context: {
        linkModifier: 'primary',
        linkLabel: 'Link button'
    },
    variants: [
        {
            name: 'primary',
            context: {
                linkModifier: 'primary'
            }
        }, {
            name: 'ghost',
            context: {
                linkModifier: 'ghost'
            }
        }
    ]
};
