module.exports = {
    //title: 'Link with icon',
    status: 'wip',

    collated: false,
    default: 'arrow-right',
    context: {
        linkLabel: 'Link with icon',
        url: '#'
    },
    variants: [
        {
            name: 'arrow-right',
            context: {
                modifier: 'arrow-right',
                linkLabel: 'Link arrow-right'
            }
        }, {
            name: 'caution',
            context: {
                modifier: 'caution',
                linkLabel: 'Link caution'
            }
        }, {
            name: 'download',
            context: {
                modifier: 'download',
                linkLabel: 'Link download',
                title: 'title="Startet Datei Download"',
                attribute: 'download'
            }
        }, {
            name: 'email',
            context: {
                modifier: 'email',
                linkLabel: 'Link email',
                title: 'title="Öffnet E-Mail"',
                url: 'mailto:dev@example.org'
            }
        }, {
            name: 'extern',
            context: {
                modifier: 'extern',
                linkLabel: 'Link extern',
                title: 'title="Öffnet externen Link in neuem Fenster"',
                target: 'target="_blank"'
            }
        }, {
            name: 'faq',
            context: {
                modifier: 'faq',
                linkLabel: 'Link info'
            }
        }, {
            name: 'info',
            context: {
                modifier: 'info',
                linkLabel: 'Link info'
            }
        }, {
            name: 'intern',
            context: {
                modifier: 'intern',
                linkLabel: 'Link intern',
                title: 'title="Öffnet internen Link in neuem Fenster"'
            }
        }
    ]
};
