module.exports = {
    //title: 'Logo',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        logoModifier: 'u-typo:xl',
        logoLink: '/components/preview/homepage',
        logoImg: '/typo3conf/ext/fgp_template/Resources/Public/Frontend/Default/icons/logo.svg'
    }
};
