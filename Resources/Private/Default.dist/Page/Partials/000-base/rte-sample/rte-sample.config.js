module.exports = {
    //title: 'RTE Sample',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        textM: '@data.text.paragraph-m',
        textL: '@data.text.paragraph-l',
        linkData: {
            linkLabel: 'this is an inline link'
        }
    }
};
