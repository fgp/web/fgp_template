module.exports = {
    title: 'Site Header',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        siteHeaderModifier: 'u-typo:xl',
        siteHeaderLink: '/',
        siteHeaderText: 'Freie Grundschule Pfefferwerk'
    }
};
