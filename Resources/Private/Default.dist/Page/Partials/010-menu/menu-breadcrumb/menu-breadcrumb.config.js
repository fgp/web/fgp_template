module.exports = {
    //title: 'Breadcrumb',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        spaceAfter: '@data.spaceAfter.small',
        breadcrumbItems: ['Start', 'Unterseite'],
        breadcrumbCurrent: 'Aktuelle Seite'
    }
};
