module.exports = {
    //title: 'Dropdown',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        buttonClass: false,
        dropdownLabel: 'Klick Me',
        dropdownItems: [
            {
                dropdownLink: '@data.text.headline-s'
            }, {
                dropdownLink: '@data.text.headline-m'
            }, {
                dropdownLink: '@data.text.headline-s'
            }
        ]
    }
};
