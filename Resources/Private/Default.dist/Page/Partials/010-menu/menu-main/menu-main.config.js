module.exports = {
    //title: 'Menu Main',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        menuMainItems: [
            {
                current: true,
                title: 'Schule',
                url: '#'
            },
            {
                title: 'Konzept',
                url: '#'
            }, {
                title: 'Team',
                url: '#'
            }, {
                title: 'Träger',
                url: '#'
            }, {
                title: 'Formelles',
                url: '#'
            }
        ]
    }
};
