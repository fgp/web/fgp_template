module.exports = {
    //title: 'Menu Meta',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        headline: 'Menu Meta',
        ariaLabel: 'Meta-Navigation',
        menuMetaItems: [
            {
                current: true,
                title: '@data.text.headline-s',
                url: '#'
            },
            {
                title: '@data.text.headline-s',
                url: '#'
            }
        ]
    }
};
