module.exports = {
    //title: 'Menu Skip',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        menuSkipItems: [
            {
                title: 'Direkt zur Hauptnavigation',
                url: '#main-menu'
            }, {
                title: 'Direkt zum Hauptinhalt',
                url: '#main-content'
            }
        ]
    }
};
