module.exports = {
    //title: 'Menu Sub',
    status: 'wip',

    collated: false,
    default: 'default',
    context: {
        spaceAfter: '@data.spaceAfter.default',
        headline: 'Menu Sub',
        menuSubItems: [
            {
                active: true,
                current: true,
                title: '@data.text.headline-m',
                titleCurrent: '@data.text.headline-s',
                url: '#',
            }, {
                title: '@data.text.headline-m',
                url: '#'
            }, {
                title: '@data.text.headline-m',
                url: '#'
            }, {
                title: '@data.text.headline-m',
                url: '#'
            }
        ]
    }
};
