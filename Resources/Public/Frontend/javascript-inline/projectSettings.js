window.projectSettings = {
    config: {
        system: {
            code: 'lab', // lab/dev/stage/live (CMS application context)
            baseUrl: 'http://fgp-lab.freie-grundschule.de' // CMS/PL baseUrl
        }
    },
    // if necessary
    locallang: {
        more: 'more'
    }
}
