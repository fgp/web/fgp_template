<?php

/**
 * Extension Manager/Repository config file for ext "fgp_template".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'FGP Template',
    'description' => 'Template provider extension for freie-grundschule.de',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-9.5.99',
            'rte_ckeditor' => '8.7.0-9.5.99',
            'bootstrap_package' => '10.0.0-10.0.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Dwenzel\\FgpTemplate\\' => 'Classes'
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Dirk Wenzel',
    'author_email' => 'der-wenzel@gmx.de',
    'author_company' => 'DWenzel',
    'version' => '1.0.0',
];
