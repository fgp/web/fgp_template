<?php

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['fgp_template'] = 'EXT:fgp_template/Configuration/RTE/Default.yaml';

/***************
 * PageTS
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TsConfig/Page/All.tsconfig">');

/**
$GLOBALS['TYPO3_CONF_VARS']['EXT']['twig_for_typo3']['twig']['loader'][] = \DWenzel\FgpTemplate\Twig\Loader\ResourcePathLoader::class;
$GLOBALS['TYPO3_CONF_VARS']['EXT']['twig_for_typo3']['twig']['loader'][] = \DWenzel\FgpTemplate\Twig\Loader\RecursivePathLoader::class;
 */