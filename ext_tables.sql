#
# Table structure for table 'pages'
#
CREATE TABLE pages (
  sitepackage_metatitle varchar(255) default NULL,
  sitepackage_pageheadline varchar(255) default NULL,
  sitepackage_pageteaser text NOT NULL,
);

#
# Table structure for table 'pages_language_overlay'
#
CREATE TABLE pages_language_overlay (
  sitepackage_metatitle varchar(255) default NULL,
  sitepackage_pageheadline varchar(255) default NULL,
  sitepackage_pageteaser text NOT NULL,
);
